function heredoc (f) {return f.toString().match(/\/\*\s*([\s\S]*?)\s*\*\//m)[1];};var wforecast_tops_0 = heredoc(function(){/*<style>
        .wforecast_tops_0 {
            background-color: #f5f6f8;
            border-top: 4px solid #2a303b;
            padding: 0 20px 20px 20px;
        }
            .wforecast_tops_0-articles-type {
                font-family: "Marr Sans Condensed",DINCondensed-Bold,"Roboto Condensed","Arial Narrow",sans-serif;
                font-size: 28px;
                line-height: 1.4;
                font-weight: 700;
                margin: 0;
                margin-top: 28px;
                color: #2a303b;
            }
            .wforecast_tops_0-articles {
                margin: 0;
                padding: 0;
                background-color: hsl(220, 18%, 97%);
                list-style: none;
            }
                .wforecast_tops_0-article:not(:last-child) {
                    border-bottom: 1px solid #d5d8dc;
                }
                .wforecast_tops_0-article a {
                    display: flex;
                    padding: 24px 0;
                    text-decoration: none;
                    color: #2a303b;
                }
                    .wforecast_tops_0-article-number {
                        font-family: "The Antiqua B",Georgia,Droid-serif,serif;
                        margin-top: -10px;
                        display: flex;
                        font-weight: 600;
                        line-height: normal;
                        color: #2a303b;
                        font-size: 32px;
                    }
                    .wforecast_tops_0-article-title  {
                        font-family: "The Antiqua B",Georgia,Droid-serif,serif;
                        display: flex;
                        color: #2a303b;
                        font-size: 18px;
                        line-height: 1.3;
                        letter-spacing: -0.02px;
                        font-weight: 400;
                        padding-left: 16px;
                    }
                        .wforecast_tops_0-article a:hover .wforecast_tops_0-article-title {
                            text-decoration: underline;
                        }
    </style><div class="wforecast_tops_0">  <p class="wforecast_tops_0-articles-type">Les plus lus</p> <ul class="wforecast_tops_0-articles"><li class="wforecast_tops_0-article"> <a href="https://www.lemonde.fr/pixels/article/2020/02/14/l-itineraire-des-videos-qui-ont-pousse-benjamin-griveaux-a-renoncer-a-la-mairie-de-paris_6029613_4408996.html?forecast_method=widget&forecast_data=top_article_list"> <span class="wforecast_tops_0-article-number">1</span> <span class="wforecast_tops_0-article-title">L’itinéraire des vidéos qui ont poussé Benjamin Griveaux à renoncer à la Mairie de Paris</span> </a> </li><li class="wforecast_tops_0-article"> <a href="https://www.lemonde.fr/politique/article/2020/02/14/benjamin-griveaux-renonce-a-la-mairie-de-paris-apres-la-diffusion-d-images-privees-a-caractere-sexuel_6029533_823448.html?forecast_method=widget&forecast_data=top_article_list"> <span class="wforecast_tops_0-article-number">2</span> <span class="wforecast_tops_0-article-title">Benjamin Griveaux renonce à la Mairie de Paris après la diffusion de vidéos intimes à caractère sexuel</span> </a> </li><li class="wforecast_tops_0-article"> <a href="https://www.lemonde.fr/politique/article/2020/02/15/la-macronie-secouee-par-le-seisme-politique-de-l-affaire-benjamin-griveaux_6029654_823448.html?forecast_method=widget&forecast_data=top_article_list"> <span class="wforecast_tops_0-article-number">3</span> <span class="wforecast_tops_0-article-title">La macronie secouée par le « séisme politique » de l’affaire Benjamin Griveaux</span> </a> </li></ul>  </div>*/}); 