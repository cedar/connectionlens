Une prime de 600 euros pour les policiers qui verbaliseront le plus d’automobilistes !
				
Selon une étude révélée par Auto Plus, les compagnies de CRS 
autoroutières et les compagnies de motards sanctionnant le plus 
toucheraient une prime de motivation de 600 euros.

Les neuf compagnies autoroutières et les vingt-deux unités de motards
 sont récompensées en fonction de leurs performances. En effet, des 
points leur sont attribués pour celles qui dressent le plus 
d’infractions avec pertes de points sur les permis de conduire, les 
excès de vitesse, les autres délits ou encore les entraînements au tir 
des policiers

Le cumul des points sert à déterminer les deux unités
 et les deux compagnies les plus efficaces en vue de leur accorder une 
prime annuelle, dont le montant est «équitablement partagé entre les
agents du service», confirme Auto Plus.

C’est toujours la politique du chiffre qui prime, 
avec les méfaits que ça engendre: la répression est privilégiée 
sur la prévention. Une méthode révoltante puisqu’elle empêche le 
policier de faire preuve de discernement et le prive de sa libre 
intervention sous peine de se faire remonter les bretelles»,
 explique Phillipe Capon, secrétaire général du syndicat UNSA Police, 
en ajoutant que le PV est devenu l’un des critères les plus importants 
en vue de la notation et de la carrière du fonctionnaire.

Le résultat? L’automobiliste ne sait plus 
pourquoi il est verbalisé, il ne peut pas bien se défendre. Les 
contentieux augmentent fortement. Cette méthode n’améliore évidemment 
pas les relations avec la population, elle modifie le comportement des 
usagers, les pousse à se soustraire au contrôle», conclut-il.
