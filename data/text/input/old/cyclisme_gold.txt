Cyclisme : le cas <PERSON>Froome</PERSON> ne sera pas tranché avant plusieurs mois
9-11 minutes

Selon nos informations, <PERSON>Christopher Froome</PERSON> ne passera pas dans l’immédiat l’examen censé prouver sa bonne foi après son contrôle antidopage anormal. Mais son dossier s’annonce solide.

<ORGANIZATION>LE MONDE</ORGANIZATION> | 14.12.2017 à 10h02 • Mis à jour le 14.12.2017 à 10h36 | Par <PERSON>Clément Guillou</PERSON>
<PERSON>Christopher Froome</PERSON>, ici le 13 décembre, est actuellement en stage à <LOCATION>Majorque</LOCATION>, avec son équipe <ORGANIZATION>Sky</ORGANIZATION>.
<PERSON>Christopher Froome</PERSON>, ici le 13 décembre, est actuellement en stage à <LOCATION>Majorque</LOCATION>, avec son équipe <ORGANIZATION>Sky</ORGANIZATION>. <PERSON>JOAN LLADO</PERSON> / <ORGANIZATION>AP</ORGANIZATION>

Le cyclisme mondial va vivre une partie de l’année 2018 dans l’incertitude d’une décision concernant son plus célèbre représentant, le quadruple vainqueur du Tour de France : selon les informations du <ORGANIZATION>Monde</ORGANIZATION> et du quotidien britannique <ORGANIZATION>The Guardian</ORGANIZATION>, le cas de <PERSON>Christopher Froome</PERSON>, contrôlé le 7 septembre à un taux de salbutamol anormalement élevé, ne sera pas tranché avant plusieurs mois.

Lire notre enquête :   Cyclisme : contrôle antidopage « anormal » pour <PERSON>Chris Froome</PERSON>

Selon une source proche du dossier, l’équipe juridique et scientifique dont s’est entouré le Britannique de l’équipe <ORGANIZATION>Sky</ORGANIZATION> n’a pas encore procédé à l’étude en laboratoire censée prouver la bonne foi de <PERSON>Christopher Froome</PERSON>. Un signe de l’extrême précaution avec laquelle elle est préparée, puisque la nouvelle de la procédure remonte maintenant à douze semaines.

L’aréopage d’experts est encore dans l’attente d’éléments précis pouvant l’aider à comprendre comment, autour du 7 septembre, a fonctionné le métabolisme de <PERSON>Christopher Froome</PERSON> pour qu’un produit se soit subitement retrouvé en quantité si importante dans son corps alors qu’il est censé en connaître parfaitement le fonctionnement.

Lire notre compte-rendu de tchat :   « <PERSON>Froome</PERSON> essaye de prouver qu’il a agi en toute bonne foi »

Avec l’imminence des vacances de fin d’année et l’organisation que nécessite cette étude, il est peu probable qu’elle ait lieu avant 2018. Il faudra ensuite attendre de connaître son résultat, défendre le cas devant le <ORGANIZATION>tribunal antidopage</ORGANIZATION> de l’<ORGANIZATION>Union cycliste internationale(UCI)</ORGANIZATION> et attendre sa décision. Pourrait suivre un appel de <PERSON>Froome</PERSON> ou de l’<ORGANIZATION>Agence mondiale antidopage (AMA)</ORGANIZATION> devant le <ORGANIZATION>tribunal arbitral du sport (TAS)</ORGANIZATION> : il est probable que le cas poursuive<PERSON>Froome</PERSON> et le cyclisme au printemps, y compris pendant le Tour d’Italie, dont il est censé être la vedette.

L’Italien <PERSON>Diego Ulissi</PERSON>, qui avait passé à l’été 2014 une étude en laboratoire après un contrôle anormal au salbutamol, n’avait connu sa suspension qu’en janvier 2015.

Lire aussi :   De nombreux sportifs ont été épinglés à cause du salbutamol
Déshydratation

Cette étude pharmacocinétique doit expliquer pourquoi, au soir d’une étape de montagne de son Tour d’Espagne victorieux, <PERSON>Christopher Froome</PERSON> a rendu un échantillon d’urine présentant 2 000 nanogrammes de salbutamol par millilitre (ng/ml). Tout en ayant, selon ses dires, respecté des doses de Ventoline ne dépassant pas 800 microgrammes toutes les douze heures – soit huit bouffées –, qui auraient dû, au maximum, porter ce taux à 1 000 ng/ml.

L’entourage du quadruple vainqueur du Tour compte s’appuyer sur deux éléments scientifiques. Le premier est une possible déshydratation, dont plusieurs études scientifiques ont montré qu’elle pouvait perturber l’excrétion du salbutamol, et provoquer le dépassement des seuils fixés par l’<ORGANIZATION>AMA</ORGANIZATION>. Toutefois, il ne faisait pas particulièrement chaud le 7 septembre, étape où le peloton avait roulé maillots bien fermés sous le ciel bas de <LOCATION>Cantabrie</LOCATION>.

Le deuxième concerne d’autres facteurs physiologiques ayant pu perturber le métabolisme du salbutamol lors de son passage dans l’organisme de <PERSON>Christopher Froome</PERSON> : en effet, le mélange avec d’autres substances médicamenteuses ou alimentaires peut lui aussi modifier l’excrétion du salbutamol.
Discrétion et opacité

Compte tenu de ce retard au démarrage, les révélations du <ORGANIZATION>Monde</ORGANIZATION> et du <ORGANIZATION>Guardian</ORGANIZATION> tombent bien mal pour l’<ORGANIZATION>UCI</ORGANIZATION et l’équipe la plus puissante du sport, la <ORGANIZATION>Team Sky</ORGANIZATION>, qui s’attendaient à pouvoir gérer le cas dans la discrétion jusqu’à sa résolution. Et <PERSON>Christopher Froome</PERSON> à courir comme si de rien n’était, comme il le fit le 20 septembre en prenant la troisième place des championnats du monde contre la montre quelques heures après avoir appris l’existence de ce contrôle anormal.

Une fois de plus, la <ORGANIZATION>Team Sky</ORGANIZATION> a tiré le plus grand profit de sa non-appartenance au mouvement pour un cyclisme crédible (<ORGANIZATION>MPCC</ORGANIZATION>) : les règles internes de ce groupe d’équipes, auquel appartiennent 7 des 18 formations de l’élite et la quasi-totalité de celles de deuxième division, auraient obligé l’équipe britannique à suspendre provisoirement son coureur le temps de la procédure. Les Italiens de <LOCATION>Lampre</LOCATION> l’avaient fait, en juin 2014, pour <PERSON>Diego Ulissi</PERSON>, pourtant l’un de leurs meilleurs coureurs, dès la positivité de l’échantillon A.

Depuis plusieurs années, toutefois, la transparence ne guide plus les actions de <ORGANIZATION>Sky</ORGANIZATION>, qui s’était construite en 2010 sur une promesse d’éthique et de rupture avec le passé du cyclisme et ses arrangements à l’amiable. La démonstration a été faite en 2016, lorsque l’enquête des parlementaires britanniques et de l’<ORGANIZATION>agence antidopage nationale</ORGANIZATION> (<ORGANIZATION>UKAD</ORGANIZATION>) s’est heurtée à l’absence de factures d’achats de médicaments et au silence du docteur <PERSON>Richard Freeman</PERSON>, alors que des soupçons d’utilisation de corticoïdes en compétition et de testostérone pesaient sur la <ORGANIZATION>Team Sky</ORGANIZATION>.

Une promesse de départ de <ORGANIZATION>Sky</ORGANIZATION>, que l’équipe britannique a jusqu’ici tenue, concerne le licenciement automatique de tout coureur suspendu pour dopage. En cas de suspension de <PERSON>Froome</PERSON>, même de trois mois, elle n’aura que deux choix : se renier une nouvelle fois ou se séparer de son porte-drapeau. Aucune de ces deux décisions ne laisserait indifférent l’opérateur de télévision par satellite, propriétaire de la licence de l’équipe.
Trois bouffées après l’étape ?

Mercredi matin, l’équipe britannique a communiqué publiquement six minutes après avoir répondu aux questions du <ORGANIZATION>Monde</ORGANIZATION> et du <ORGANIZATION>Guardian</ORGANIZATION>. Une tentative de reprendre la main sur l’affaire. Ce communiqué fait porter la responsabilité à un docteur qui n’est pas nommé : <PERSON>Derick MacLeod</PERSON>, selon les informations du <ORGANIZATION>Daily Mail</ORGANIZATION>. Engagé par <ORGANIZATION>Sky</ORGANIZATION> depuis 2015, cet Ecossais est spécialiste des sports d’hiver et de football, puisqu’il travaille fréquemment avec l’équipe nationale écossaise.

L’entourage de <PERSON>Froome</PERSON> a laissé sortir l’information, dans le <ORGANIZATION>Times</ORGANIZATION>, selon laquelle le docteur <PERSON>MacLeod</PERSON> aurait dit au maillot rouge de la Vuelta de prendre trois bouffées de Ventoline après l’étape et avant le contrôle antidopage. Un conseil étrange à un coureur qui, ce soir-là dans son interview d’après-course, ne toussait pas et se disait « redevenu [lui]-même » après une « bien meilleure journée » que la veille.

Ce schéma de prise de salbutamol sera sans aucun doute répété lors de l’étude pharmacocinétique à laquelle se prêtera <PERSON>Christopher Froome</PERSON>.

« Le contexte est plus éclairant que le dépassement du seuil »

Cette étude est censée reproduire les conditions du jour du contrôle anormal dans un laboratoire, en présence de témoins qui s’assurent que le sportif ne prend pas une dose de salbutamol trop élevée, explique au <ORGANIZATION>Monde</ORGANIZATION> le docteur <PERSON>Olivier Rabin</PERSON>, directeur scientifique de l’<ORGANIZATION>AMA</ORGANIZATION>.

    « Il nous dit : “Le jour où j’ai été contrôlé positif, voilà comment j’ai pris la substance incriminée, voilà combien j’en ai pris et à quel moment.” On fait un prélèvement urinaire sans prise de la substance, pour avoir le niveau basal de l’athlète, surtout quand ce sont des athlètes qui ont des traitements chroniques. Ensuite, l’athlète prend le salbutamol dans les conditions qu’il décrit avoir pris. Et ensuite on vous prélève à des heures relativement précises. Il est relativement difficile de fausser ce genre d’analyse. »

Si <PERSON>Olivier Rabin</PERSON> refuse de commenter un cas en cours, il livre des éléments de nature à conforter <PERSON>Christopher Froome</PERSON> :

    « Il y a une différence entre un athlète qui va se retrouver avec du salbutamol alors qu’il n’a pas d’autorisation d’usage thérapeutique et celui qui a été sous salbutamol pendant des années et se retrouve avec un dépassement de seuil, pour lequel une étude d’excrétion va montrer qu’il a un métabolisme un peu différent de la norme sur cette substance. (…) Les circonstances et le contexte sont plus éclairants que la concentration et le dépassement du seuil [conséquent en ce qui concerne <PERSON>Chris Froome</PERSON>]. »

En attendant de côtoyer des laborantins, le Britannique continue de préparer sa saison à <LOCATION>Majorque</LOCATION> avec son équipe. Mercredi soir, il ne s’est pas défilé devant une interview prévue à l’avance avec la <ORGANIZATION>BBC</ORGANIZATION>. Le journaliste de la télévision publique britannique lui a demandé si son héritage serait terni par cette affaire. <PERSON>Froome</PERSON> a répondu d’un « non » ferme : « Je comprends que cela choque les gens. Mais je n’ai enfreint aucune règle. Au final, on connaîtra la vérité. »
