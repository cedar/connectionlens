Chancel et Piketty : « A un moment, les contribuables captifs finissent par se rebeller »
7-8 minutes

Pour les auteurs du « Rapport sur les inégalités mondiales 2018 », « les tendances inégalitaires des trente dernières années ont pris des proportions excessives et néfastes ».
Thomas Piketty et Lucas Chancel, à Paris, le 7 décembre.
Thomas Piketty et Lucas Chancel, à Paris, le 7 décembre. JULIEN FAURE/ LEEXTRA POUR LE MONDE

Lucas Chancel et Thomas Piketty, économistes à l’Ecole d’économie de Paris (PSE), et auteurs du « Rapport sur les inégalités mondiales 2018 », expliquent, d’une seule voix, leur démarche.

Pourquoi vous êtes-vous lancés dans cette vaste étude sur les inégalités dans le monde ?

Il existe une forte demande de participation aux débats économiques et un certain désarroi face à des notions comme le produit intérieur brut [PIB] ou la croissance. Ces indicateurs sont très éloignés de ce que les gens voient autour d’eux. Cela crée une défiance envers l’économie et le processus politique.

Quels groupes sociaux ont vu croître leurs revenus ces dernières années ? Comment la richesse se répartit-elle à travers le monde ? On a envie de savoir. Cette analyse n’avait pas été faite jusqu’ici, en tout cas pas de façon statistique.

Pourtant, la Banque mondiale, le Fonds monétaire international (FMI) voire l’Organisation des Nations unies (ONU) font des études sur le sujet ?

Nos données fiscales sont beaucoup plus parlantes que les enquêtes déclaratives des organisations internationales qui sous-estiment considérablement les revenus des plus aisés. Elles laissent croire, par exemple, que les plus riches ne gagnent pas plus de trois fois le salaire moyen. Ce n’est pas crédible.

Nos données montrent que la tendance inégalitaire des trente dernières années a pris des proportions excessives et néfastes. Les organisations internationales se sont intéressées aux pauvres, pas aux riches.

Thomas Piketty, la publication de votre ouvrage, « Le capital au XXIe siècle » (Le Seuil), en 2013, a-t-elle facilité ces recherches ?

Ce livre, et c’est une de ses limites, était très centré sur les pays occidentaux. Son succès a permis de forcer l’accès à des fichiers fiscaux que des gouvernements ne voulaient pas transmettre, comme au Brésil, en Corée du Sud, en Afrique du Sud, et même en Chine d’une certaine façon. Maintenant, notre cartographie des trajectoires inégalitaires dans le monde ne couvre plus seulement les pays développés mais aussi un certain nombre d’émergents.

Mais peut-on vraiment comparer des statistiques chinoises et allemandes ?

On le fait déjà pour le PIB ! Tout le débat sur la mondialisation est surdéterminé par cette statistique, alors que ce qu’il y a sous le capot laisse vraiment à désirer. Une partie de notre projet consiste à la remettre en cause.

Heureusement, c’est plus facile de collecter des données à grande échelle aujourd’hui que ça ne l’était il y a vingt ou trente ans. Dans les années 1950, Simon Kuznets, un des premiers chercheurs à se pencher sur ces questions, développait ses statistiques à la main.

Vous parvenez à une « courbe de l’éléphant » montrant que les classes moyennes occidentales sont les premières victimes du phénomène. Cela, on le savait déjà…

Oui, mais on ne savait pas que, depuis le début des années 1980, le 1 % le plus aisé avait capté 27 % de la croissance totale des revenus, soit deux fois plus que les 50 % les plus pauvres. Ça oblige à se poser la question : était-ce indispensable d’avoir une telle explosion en haut de la distribution ? Les pays qui ont la plus forte croissance des inégalités ont-ils aussi la plus forte croissance en matière d’innovation ? La réponse est non.

Ces trajectoires inégalitaires sont-elles corrélées à l’essor de la mondialisation ?

Les coupables, ce ne sont pas les échanges commerciaux en tant que tels, mais les politiques publiques. La progressivité fiscale a été particulièrement mise à mal ces trente dernières années. Aux Etats-Unis, par exemple, l’imposition des plus hauts revenus au niveau fédéral a été presque divisée par trois sous Reagan avant de se stabiliser. Cela a profondément modifié la répartition des revenus dans le pays. Parallèlement, le pouvoir d’achat du salaire minimum américain a baissé de 25 % en cinquante ans ! C’est un échec terrible.

En revanche, l’Europe est la région la moins inégalitaire. Faut-il s’en réjouir ?

Elle pourrait faire mieux. Même si la plupart des pays européens ont préservé les systèmes de protection sociale hérités de l’après-guerre, on n’a pas su réguler le libre-échange au sein de l’Union européenne par de la solidarité fiscale et une mise à contribution des premiers bénéficiaires de la mondialisation et de l’intégration économique.

C’est l’Europe qui a mené la danse en matière de concurrence fiscale. Donald Trump, dont l’une des réformes prévoit une baisse de l’impôt sur les sociétés à 22 %, ne fait que suivre le mouvement. Ce genre de politique mine le consentement à l’impôt et crée un ressentiment dans les classes populaires et moyennes. A un moment, les contribuables captifs finissent par se rebeller. Cela se traduit par une montée insidieuse du sentiment antimondialisation et anti-Europe.

Existe-t-il des régions épargnées par cette montée des inégalités ?

Les pays dans lesquels les inégalités sont les plus stables depuis les années 1980 sont aussi ceux où elles atteignent depuis longtemps des niveaux très élevés. C’est le cas au Brésil, en Afrique du Sud ou au Moyen-Orient. Ces territoires ont été épargnés par les chocs extrêmes sur les revenus et le patrimoine qu’ont été les deux guerres mondiales et les crises économiques. Ils n’ont pas connu d’Etat-providence, de phase de nationalisation, de mise en place d’un système de protection sociale, de fort impôt sur le revenu et sur le patrimoine et notamment sur l’héritage.

Y a-t-il finalement un niveau optimal d’égalité ?

S’il existe, on ne le connaît pas, et ce n’est pas notre rôle en tant que chercheurs de le définir. On atteint tout de même dans certains pays des niveaux extrêmes d’inégalités. Celui observé dans les années 1950-1980 en Europe et aux Etats-Unis était sans doute un assez bon compromis. Rien ne montre que ce modèle ne permettait pas la croissance. Encore une fois : la montée des inégalités n’était pas indispensable à la croissance.

Quel impact espérez-vous avoir ?

La publication de la première version de notre travail a déjà eu un impact sur ce que fait le FMI. Il se met à utiliser nos données d’il y a cinq ans. Il y a de vrais enjeux politiques. Un Etat qui n’aurait que des impôts indirects comme la TVA ne connaîtrait rien aux revenus de ses citoyens. Dans les pays taxant séparément les revenus du capital et du travail, l’appareil statistique devient illisible. Comme l’administration n’a plus besoin de faire le lien entre le salaire et les intérêts ou dividendes touchés par une personne pour calculer l’impôt, les données disparaissent. L’impôt, c’est aussi une forme de transparence.
