﻿François Hollande espère que les Français le jugeront sans oublier le contexte de son mandat

Dans un entretien à « L'Obs », le président de la République, qui considère avoir mené une politique de gauche, « se dit prêt à l'inventaire ».

A huit mois de la présidentielle, François Hollande livre un message de moins en moins équivoque quant à son bilan et en vue d'une éventuelle candidature à l'élection présidentielle. Dans un entretien à L'Obs à paraître jeudi 13 octobre, il se choisit un adversaire, Alain Juppé, et se dépeint comme un homme de gauche.

« Prêt à l'inventaire »

Les Français « vont pouvoir comparer non pas ce que j'ai fait à l'aune de ce que j'avais promis », mais « ce que j'ai fait dans le contexte que chacun connaît avec ce que proposent ceux qui prétendent nous remplacer », déclare ainsi François Hollande.

Le président de la République se dit toutefois « prêt à l'inventaire » de ses 60 engagements de la campagne de 2012, assurant avoir mené une politique de gauche. Il cite les 60 000 postes créés dans l'éducation nationale, la réduction des « inégalités fiscales » et les « avancées sociales » au cours de son mandat. Le président de la République défend également la loi El Khomri, « une loi sociale » avec des mesures « de gauche ».
Déchéance de nationalité, regrets sur le débat

Interrogé sur le projet de révision de la Constitution pour y inscrire la déchéance de nationalité dans la Constitution pour les auteurs d'actes de terrorisme, le chef de l'Etat esquisse un mea culpa. Il dit comprendre « le trouble que cette initiative a pu créer ».

François Hollande reconnaît que la mesure n'avait « aucune valeur dissuasive » et explique l'avoir défendue car elle s'inscrivait « dans un plan d'ensemble pour unir le pays ». Le regret du président ? « Ne pas avoir réussi à nous donner un cadre commun sur ces questions (…) Je regrette que la gauche l'ait regardée comme une mesure qui pouvait diviser. Je regrette que l'opposition en ait fait un sujet de surenchère politique. »
Ses adversaires à gauche étrillés

A propos de ses anciens ministres, candidats officiels ou possibles à l'élection présidentielle (Benoît Hamon, Arnaud Montebourg, Emmanuel Macron, Cécile Duflot), François Hollande, qui pourrait être leur adversaire s'il décide de briguer sa réélection, affirme que :

    « Quitter un gouvernement, alors qu'ils ont l'opportunité de changer la France, d'avoir les leviers en main pour peser sur la politique économique ou industrielle, sauver des emplois, redresser des secteurs entiers, s'atteler aux problèmes du décrochage scolaire ou du logement, eh bien je pense que c'est une forme d'oubli de ce qu'est le sens de la vie politique. »

Un adversaire ciblé à droite : Alain Juppé

Le chef de l'Etat est également interrogé sur le projet d'électeurs de gauche d'aller voter à la primaire de la droite afin de barrer la route à Nicolas Sarkozy. François Hollande espère les détourner de ce dessein. « Le premier devoir d'un électeur, c'est de faire valoir ses idées. Je pense que si nous installons l'idée que pour éviter l'extrême droite il faut voter pour la droite, eh bien, à ce moment-là, il n'y aura plus de gauche », argue-t-il.

Il fustige l'« identité heureuse », thème de campagne d'Alain Juppé. Pour le chef de l'Etat, « les deux mots sont mal choisis. "Identité”, parce que la France est renvoyée à son passé alors que c'est l'idée de la France qui constitue son histoire et son avenir. Et "heureuse” parce que c'est un malentendu ».

Lui entend rappeler les fondamentaux du discours de la droite au terme d'un quinquennat où il lui a été souvent reproché de naviguer dans les eaux idéologiques de ses adversaires politiques. « La droite est dans la revanche, la réaction et la régression. C'est le programme commun de tous ces candidats. (…) Le "programme commun de la droite” ne consiste pas à défaire ce qui a été fait depuis cinq ans mais à revenir sur des droits et des principes qui ont mis des décennies pour être conquis et acquis », accuse encore le chef de l'Etat.
