# About us

## Senior Authors
- **Angelos Anadiotis**, Assistant Professor at Ecole Polytechnique, CEDAR team.
- **Oana Balalau**, Starting Researcher (Inria) and part-time Associate Professor at Ecole Polytechnique, CEDAR team.
- **Helena Galhardas**, Senior researcher (INESC-ID, Portugal) and Associate Professor at IST, Universidade de Lisboa.
- **Julien Leblay** (AIST researcher, now at Rakuten) (2017-2019).
- **[Ioana Manolescu](https://pages.saclay.inria.fr/ioana.manolescu/) (project lead)**, Senior researcher (Inria) and part-time Professor at Ecole Polytechnique, CEDAR team.
- **Tayeb Merabti**, Senior Research Engineer at Inria, CEDAR team (2019-2020).
- **Emmanuel Pietriga**, Senior researcher (Inria), ILDA team.

## Student contributors

- **Irène Burger**, Inria M1 intern, Ecole Polytechnique, 2020
- **Camille Chanial**, AIST Japan M1 intern, Ecole Polytechnique, 2018
- **Catarina Conceiçao**, M2 engineering student, University of Lisbon, Portugal, 2018-2020
- **Jérémie Feitz**, Inria M1 intern, Ecole Polytechnique, 2020
- **Minh-Huong Le Nguyen**, Inria M1 intern, Université de Paris Saclay and Ecole Polytechnique, 2018
- **Yamen Haddad**, M2 intern/junior engineer, Université de Paris Saclay, 2020
- **Youssr Youssef**, M2 intern, ENSAE and Sciences Po, 2020

Other students who worked on the project in the past are: Rédouane Dziri (M1, Ecole Polytechnique, 2018), Lucas Elbert (M1, Ecole Polytechnique, 2019), Felipe Cordeiro (U. Sao Paulo, 2019).
Jingmao You (M2, Ecole Polytechnique, 2020) developed a disambiguation Web Service that ConnectionLens exploits. 

