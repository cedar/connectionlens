import random
import argparse
import json
import pandas as pd

relationships = ["friendOf", "marriedWith", "worksWith", "familyMemberOf", "studiedWith"]
people = []
companies = []
locations = []
dummy_sentences = []
last_names = []
first_names = []
file_base_name = "generated-scalability-analysis-"


def pick(l):
	return l[random.randint(0, len(l) - 1)]


def uri(s):
	return "<http://example.org/" + s.replace(" ", "_") + ">"


def load_companies(filename):
	companies.clear()
	with open(filename, 'r') as f:
		for line in f:
			name, ceo = line.split('\t')
			ceo = pick(first_names)
			company = {"name": name.strip(), "ceo": ceo}
			companies.append(company)
			people.append(ceo.strip())


def load_locations(filename):
	with open(filename, 'r') as f:
		for line in f:
			locations.append(line.strip())


def load_dummy_sentences(filename):
	with open(filename, 'r') as f:
		for line in f:
			dummy_sentences.append(line.strip())


def load_people(first_name_file, last_name_file):
	with open(last_name_file, 'r') as f:
		for line in f:
			last_names.append(line.strip())
	with open(first_name_file, 'r') as f:
		for line in f:
			first_names.append(line.strip())
	for i in range(int(args.scalefactor/2)):
		people.append(pick(first_names) + " " + pick(last_names))


def generate_rdf(filename):
	with open(filename, 'w') as f:
		for i in range(int(args.scalefactor*.3)):
			p1 = pick(people)
			p2 = pick(people)
			while p1 == p2:
				p2 = pick(people)
			f.write(uri(p1) + " " + uri(pick(relationships)) + " " + uri(p2) + " .\n")
			f.write(uri(p1) + " " + uri("name") + ' "' + p1 + '" .\n')
			f.write(uri(p2) + " " + uri("name") + ' "' + p2 + '" .\n')
		for i in range(int(args.scalefactor*.1)):
			p1 = pick(people)
			f.write(uri(p1) + " " + uri("livesIn") + " " + uri(pick(locations)) + " .\n")


def generate_csv(filename):
	with open(filename, 'w') as f:
		f.write("company, employee\n")
		for p in people:
			# TODO: treat CEOs cases separately
			f.write('"' + pick(companies)["name"] + '", "' + p + '"\n')


def generate_json(filename):
	full_companies = []
	for c in companies:
		c["headquarters"] = pick(locations)
		branches = []
		# for i in range(random.randint(0, int(args.scalefactor/len(companies)))):
		# 	branches.append(pick(locations))
		# c["branch_offices"]=branches
		full_companies.append(c)
	with open(filename, 'w') as f:
		f.write(json.dumps(full_companies, indent=4))


def generate_text(filename):
	text = []
	sentences = pd.read_csv('sentences.csv', sep='\t')
	for i in range(int(args.scalefactor*0.6)):
		# pick a ceo
		c = random.randint(0, len(sentences["ceo"]) - 1)
		while pd.isnull(sentences.iloc[c, 1]) or sentences.iloc[c, 1] == "[]":
			# pick a ceo
			c = random.randint(0, len(sentences["ceo"]) - 1)
		# get sentences about him
		about = sentences.iloc[c, 1].split("', '")
		s = random.randint(0, len(about) - 1)
		text.append(clean(about[s]))
	for i in range(int(args.scalefactor*0.4)):
		text.append(pick(dummy_sentences))
	random.shuffle(text, random.random)
	res = ""
	for i in range(len(text)):
		res += text[i]+" "
	with open(filename, 'w') as f:
		f.write(res)


def clean(text):
	text = text.replace("['", "")
	text = text.replace("']", "")
	if not text.endswith("."):
		text += "."
	return text


def load():
	load_dummy_sentences('french-sentences.txt')
	load_people('prenoms-from-data-gouv-fr.txt', 'lastnames.txt')
	load_locations('locations.txt')
	load_companies('companies.txt')


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-n", "--numberoffiles", default=10, type=int, help="Number of files")
	parser.add_argument("-s", "--scalefactor", default=100, type=int, help="Scale factor")
	parser.add_argument("-r", "--randomseed",  default=0,   type=int, help="Random seed")
	args = parser.parse_args()

random.seed(args.randomseed)


scalefactor = 0
for file in range(0, args.numberoffiles):
	scalefactor += int(args.scalefactor)
	load()
	generate_rdf("{}{}{}".format(file_base_name, scalefactor, ".nt"))
	generate_csv("{}{}{}".format(file_base_name, scalefactor, ".csv"))
	generate_json("{}{}{}".format(file_base_name, scalefactor, ".json"))
	generate_text("{}{}{}".format(file_base_name, scalefactor, ".txt"))
