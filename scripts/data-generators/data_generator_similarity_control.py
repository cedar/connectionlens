import random
import argparse
import json
import pandas as pd
import numpy as np

relationships = ["friendOf", "marriedWith", "worksWith", "familyMemberOf", "studiedWith"]
people        = []
companies     = []
locations     = []
dummy_sentences = []

common_JSONRel_companies = []


def generate_shared_ensembles() :
	#k = args.simfactor
	k=10
	companies_ran = [companies[i] for i in random.sample(range(len(companies)), int(k/3))]]["name"]


def pick(l):
	return l[random.randint(0, len(l) - 1)]

def uri(s):
	return "<http://example.org/" + s.replace(" ", "_") + ">"

def load_companies(filename):
	with open(filename, 'r') as f:
		for line in f:
			name, ceo = line.split('\t')
			company = {}
			company["name"]=name.strip()
			company["ceo"]=ceo.strip()
			companies.append(company)
			people.append(ceo.strip())

def load_locations(filename):
	with open(filename, 'r') as f:
		for line in f:
			locations.append(line.strip())

def load_dummy_sentences(filename):
	with open(filename, 'r') as f:
		for line in f:
			dummy_sentences.append(line.strip())


def load_people(firstname_file, lastname_file):
	lastnames  = []
	firstnames = []
	with open(lastname_file, 'r') as f:
		for line in f:
			lastnames.append(line.strip())
	with open(firstname_file, 'r') as f:
		for line in f:
			firstnames.append(line.strip())
	for i in range(int(args.scalefactor/2)):
		people.append(pick(firstnames) + " " + pick(lastnames))

def generate_rdf(filename) :
	with open(filename, 'w') as f:
		c = 1
		for i in range(int(args.scalefactor*.3)):
			p1 = pick(people)
			p2 = pick(people)
			while p1 == p2:
				p2 = pick(people)
			f.write(uri(p1) + " " + uri(pick(relationships)) + " " + uri(p2) + " .\n")
			f.write(uri(p1) + " " + uri("name") + ' "' + p1 + '" .\n')
			f.write(uri(p2) + " " + uri("name") + ' "' + p2 + '" .\n')
			if(c==3) :
				f.write(uri(p1) + " " + uri("livesIn") + " " + uri(pick(locations)) + " .\n")
				c = 0
			c+=1

def generate_csv(filename):
	with open(filename, 'w') as f:
		f.write("company, employee\n")
		for i in range(args.scalefactor) :
			p = pick(people)
			f.write('"' + pick(companies)["name"] + '", "' + p + '"\n')

def generate_json(filename):
	full_companies = []
	for i in range(int(args.scalefactor/5)) :
		c = {}
		c["name"] = pick(companies)["name"]
		c["ceo"] = pick(companies)["ceo"]
		c["headquarters"]=pick(locations)
		branches = []
		for i in range(2):
			branches.append(pick(locations))
		c["branch_offices"]=branches
		full_companies.append(c)
	with open("generated-" + str(args.scalefactor) + ".json", 'w') as f:
		f.write(json.dumps(full_companies, indent=4))

def generate_text(filename) :
	text = []
	sentences = pd.read_csv('sentences.csv',sep='\t')
	for i in range(int(args.scalefactor*0.6)) :
		c = random.randint(0, len(sentences["ceo"]) - 1) #pick a ceo
		while pd.isna(sentences.iloc[c,1]) or sentences.iloc[c,1]=="[]":
			c = random.randint(0, len(sentences["ceo"]) - 1)#pick a ceo
		about = sentences.iloc[c,1].split("', '") #get sentences about him
		s = random.randint(0, len(about) - 1)
		text.append(clean(about[s]))
	for i in range(int(args.scalefactor*0.4)) :
		text.append(pick(dummy_sentences))
	random.shuffle(text,random.random)
	res=""
	for i in range(len(text)):
		res+=text[i]+" "
	with open("generated-" + str(args.scalefactor) + ".txt", 'w') as f:
		f.write(res)

def clean(text) :
	text=text.replace("['","")
	text=text.replace("']","")
	if not text.endswith(".") :
		text+="."
	return text

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-s", "--scalefactor", default=100, type=int, help="Scale factor")
	parser.add_argument("-r", "--randomseed",  default=0,   type=int, help="Random seed")
	args = parser.parse_args()

random.seed(args.randomseed)

load_dummy_sentences('french-sentences.txt')
load_locations('locations.txt')
load_companies('companies.txt')
load_people('firstnames.txt', 'lastnames.txt')
generate_rdf("generated-"  + str(args.scalefactor) + ".nt")
generate_csv("generated-"  + str(args.scalefactor) + ".csv")
generate_json("generated-" + str(args.scalefactor) + ".json")
generate_text("generated-" + str(args.scalefactor) + ".txt")
