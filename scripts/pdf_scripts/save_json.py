# THESE SCRIPTS ARE DEVELOPED IN PDF-INTEGRATION PROJECT
import os
import json

import rdflib

PDFTABLEXTR = rdflib.Namespace("http://tableXtr.pdf/")

from utils import path


def save_json(path, data):
    """"
    saves json file

    @var path [string], path to store it in
    @var data [dict], dictionnary to be stored
    """
    with open(path, 'w') as json_file:
        json.dump(data, json_file)


def dict_from_text(text):
    """"
    dict with line number for key and line as value

    @var text [list]
    """
    return dict(zip(range(len(text)), text))


def dict_from_dict(self, text_dict):
    """"
    dict with line number for key and line as value

    @var self.directory [string], directory of pdf
    @var self.name [string], name of pdf
    @var text_dict [dict], content dictionnary

    @returns [dict], dictionnary to be saved
    """

    if bool(text_dict):
        return {
            PDFTABLEXTR.extractedFrom: rdflib.URIRef('file://' + path(self.directory, self.name)),
            "content": text_dict
        }


def json_from_dict(self, final_dict):
    """"
    saves json file of extraction

    @var final_dict [dict], dictionnary to be saved
    """
    if final_dict:
        pdf_name = self.name.replace('-decrypted', '')
        prefix = self.directory + '/' + 'extracted_files_' + pdf_name
        if not os.path.exists(prefix):
            os.makedirs(prefix)
        save_json(prefix + '/' + '{}-text.json'.format(pdf_name), final_dict)
