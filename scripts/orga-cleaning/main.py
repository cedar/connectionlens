from preprocessing_data import *
from found_loc import *
import pandas as pd
import csv
import sys
import pickle
import os
from pathlib import Path


BASE_DIR = Path(__file__).parent

## Génération des deux dctionnaires pour permettre la localisation
if os.path.exists(BASE_DIR.joinpath("dico_1.pkl")) and os.path.exists(BASE_DIR.joinpath("dico_2.pkl")):

	with open(BASE_DIR.joinpath("dico_1.pkl"), 'rb') as f:
		dico_1 = pickle.load(f)

	with open(BASE_DIR.joinpath("dico_2.pkl"), 'rb') as f:
		dico_2 = pickle.load(f)
	
else:
	print("création des deux dictionnaires")
	path_gr_locs = BASE_DIR.joinpath('gr_locs.csv')     #'path_vers_le_csv_gr_locs_csv'
	df_locs = pd.read_csv(path_gr_locs)

	df_locs = import_locs(path_gr_locs)

	df_locs = population_filtre(df_locs, df_locs['population'])

	df_locs_fusion = df_locs_names_complet(df_locs['name'], df_locs['alternatenames'])

	dico_1, dico_2 = create_dico_one_two(df_locs_fusion['name'], df_locs_fusion['terms'])

	## Sauvegarde des dico_1 et dico_2 en tant que pickles
	with open(BASE_DIR.joinpath("dico_1.pkl"), 'wb') as tf:
		pickle.dump(dico_1, tf)

	with open(BASE_DIR.joinpath("dico_2.pkl"), 'wb') as tf:
		pickle.dump(dico_2, tf)


print("Les dictionnaires ont été crées")

#PATH universel vers le tableau avec les string origine et entité (path_csv_cl)
try:	
	path_csv_cl = sys.argv[1]
	df = pd.read_csv(path_csv_cl)

	## Preprocessing des données

	df['string_list'], df['entite'] = preformating_columns(df['string'], df['entite'], 0, regex = (r'(.*$)'))

	print("preprocessing des données")

	## Repérage des localisations et création de l'output final

	df, df['localisation'], df['entite'] = concat_geo_good_and_rattrapage(df, 'string_list', df['string_list'],df['entite'], dico_1, dico_2)

	print("identification des localisations")


	## Génération d'un csv CL avec le résultat de l'opération de localisation et de néttoyage (3 colonnes : string, entite et localisation)
	df.to_csv("resultat_nettoyage_localisation_csv_cl.csv", sep = "\t", index= False)

except IndexError:
	print("You did not provide a csv file")