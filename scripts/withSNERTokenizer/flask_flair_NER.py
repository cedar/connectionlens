# coding: utf-8
import predict_flair
from flask import Flask, request, jsonify
import warnings
from flair.models import SequenceTagger
import sys
import getopt


app = Flask(__name__)

@app.route('/ner')
def location_model():
    return "The language used is " + location + " and the model is located in " + MODEL_PATH.lower()


@app.route('/fner', methods=['POST'])
def fner_extract_entities():
    snippets = request.get_json()["snippets"]
    return jsonify(predict_flair.get_entities(snippets, model, props, tag_type, batch_size))


if __name__ == '__main__':
    warnings.filterwarnings("ignore")
    global batch_size, model, props, tag_type
    batch_size = 4
    location = "french"
    scripts_location = "scripts"
    options, remainder = getopt.getopt(sys.argv[1:], 'l:d:bs', ['location=', 'script_loc=', 'batch_size='])
    for opt, arg in options:
        if opt in ('-l', '--location'):
            location = arg
        elif opt in ('-d', '--script_loc'):
            scripts_location = arg
        elif opt in ('-bs', '--batch_size'):
            batch_size = int(arg)

    if location == 'french':
        MODEL_PATH = scripts_location+"/Flair_NER_tool/stacked-standard-flair-150-wikiner.pt"
        #MODEL_PATH = "integration/stacked-standard-flair-150-wikiner.pt"
        props = {
            'processors': 'tokenize',
            'lang': 'fr',
            'tokenize.options': 'strictTreebank3=true, '
                                'untokenizable=allkeep, '
                                'escapeForwardSlashAsterisk=false, '
                                'normalizeFractions=false, '
                                'normalizeAmpersandEntity=false, '
                                'invertible=true',
            'tokenize_model_path': scripts_location + '/Flair_NER_tool/stanfordnlp_resources/fr_gsd_models/fr_gsd_tokenizer.pt'
            #'tokenize_model_path':"integration/stanfordnlp_resources/fr_gsd_models/fr_gsd_tokenizer.pt"
        }
        tag_type = "label"
    else:
        MODEL_PATH = scripts_location + "/Flair_NER_tool/en-ner-conll03-v0.4.pt"
        #MODEL_PATH = "integration/en-ner-conll03-v0.4.pt"
        props = {
            'processors': 'tokenize',
            'lang': 'en',
            'tokenize.options': 'strictTreebank3=true, '
                                'untokenizable=allkeep, '
                                'escapeForwardSlashAsterisk=false, '
                                'normalizeFractions=false, '
                                'normalizeAmpersandEntity=false, '
                                'invertible=true',
            'tokenize_model_path': scripts_location + '/Flair_NER_tool/stanfordnlp_resources/en_ewt_models/en_ewt_tokenizer.pt'
            #'tokenize_model_path': "integration/stanfordnlp_resources/en_ewt_models/en_ewt_tokenizer.pt"
        }
        tag_type = "ner"

    # load model
    model = SequenceTagger.load(MODEL_PATH)

    app.run(debug=False, use_reloader=False)
