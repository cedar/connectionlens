#!/bin/bash

CL_DIR=.
CACHE_DIR=cache
TMP_DIR=tmp
TT_DIR=treetagger
TT_BIN=""
HEIDELTIME_DIR=heideltime
PYTHON_SCRIPTS_DIR=scripts

# get OS to get the relevant executables for treetagger
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux OS
  TT_BIN=binLinux;
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
  TT_BIN=binMacos
else
  # by default, we will try with Linux
  TT_BIN=binLinux
fi

echo "$OSTYPE"

usage() {
  echo "Usage: ${0} "
  exit 0
}

realpath() {
  OURPWD=$PWD
  cd "$(dirname "$1")"
  LINK=$(readlink "$(basename "$1")")
  while [ "$LINK" ]; do
    cd "$(dirname "$LINK")"
    LINK=$(readlink "$(basename "$1")")
  done
  REALPATH="$PWD/$(basename "$1")"
  cd "$OURPWD"
  echo "$REALPATH"
}

# no slash at the end, it will be happen when adding folders
CL_DIR=$(pwd)/cl-working-dir

echo "CL_DIR is :'$CL_DIR'"

mkdir -p $CL_DIR/$CACHE_DIR
mkdir -p $CL_DIR/$TMP_DIR


# configure Python venv
# Mac OSX or Linux OS
python3 -m venv $CL_DIR/cl_env
source $CL_DIR/cl_env/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt 
deactivate

mkdir -p $CL_DIR/$TT_DIR
rm -r $CL_DIR/$TT_DIR/*
mkdir -p $CL_DIR/$TT_DIR/bin
mkdir -p $CL_DIR/$TT_DIR/cmd
mkdir -p $CL_DIR/$TT_DIR/lib
mkdir -p $CL_DIR/$TT_DIR/models
mkdir -p $CL_DIR/$HEIDELTIME_DIR
mkdir -p $CL_DIR/$PYTHON_SCRIPTS_DIR

cp -R core/lib/treetagger/$TT_BIN/. $CL_DIR/$TT_DIR/bin/.
cp core/lib/treetagger/cmd/utf8-tokenize.perl $CL_DIR/$TT_DIR/cmd/.
cp -R core/lib/treetagger/lib/. $CL_DIR/$TT_DIR/lib/.
cp -R core/lib/treetagger/models/. $CL_DIR/$TT_DIR/models/.

rm -r $CL_DIR/$HEIDELTIME_DIR/*
cp core/lib/heideltime/config-heideltime-no-treetagger.props $CL_DIR/$HEIDELTIME_DIR/config-heideltime.props

rm -r $CL_DIR/$PYTHON_SCRIPTS_DIR/*
cp -R scripts/. $CL_DIR/$PYTHON_SCRIPTS_DIR/.

cp core/src/main/resources/parameter.settings $CL_DIR/local.settings

ABSOLUTEPATH_MODELS=$(realpath $CL_DIR/$TT_DIR/models)
ABSOLUTEPATH_TMP=$(realpath $CL_DIR/$TMP_DIR)
ABSOLUTEPATH_CACHE=$(realpath $CL_DIR/$CACHE_DIR)
ABSOLUTEPATH_PYTHON=$(realpath $CL_DIR/cl_env)
ABSOLUTEPATH_TT=$(realpath $CL_DIR/$TT_DIR)
ABSOLUTEPATH_HEIDELTIME=$(realpath $CL_DIR/$HEIDELTIME_DIR)
ABSOLUTEPATH_PYTHON_SCRIPTS=$(realpath $CL_DIR/scripts)

# add to local.settings the  absolute path to models
echo "\n\n#### GENERATED PARAMETERS\n" >> $CL_DIR/local.settings

# add to local.settings the absolute path to tmp directory
echo "# Temporary directory for ConnectionLens" >> $CL_DIR/local.settings
echo "temp_dir=${ABSOLUTEPATH_TMP}" >> $CL_DIR/local.settings

# add to local.settings the absolute path to the cache
echo "# ConnectionLens cache location" >> $CL_DIR/local.settings
echo "cache_location=${ABSOLUTEPATH_CACHE}" >> $CL_DIR/local.settings

# add to local.settings the absolute path to python
echo "# Python location" >> $CL_DIR/local.settings
echo "python_path=${ABSOLUTEPATH_PYTHON}/bin/python" >> $CL_DIR/local.settings

# add to local.settings the absolute path to python scripts
echo "# Path to python scripts (flair and pdf)" >> $CL_DIR/local.settings
echo "python_script_location=${ABSOLUTEPATH_PYTHON_SCRIPTS}" >> $CL_DIR/local.settings

# add to local.settings the absolute paths to TreeTagger
echo "# Treetagger location" >> $CL_DIR/local.settings
echo "treetagger_home=${ABSOLUTEPATH_TT}" >> $CL_DIR/local.settings

echo "# Stanford models location" >> $CL_DIR/local.settings
echo "stanford_models=${ABSOLUTEPATH_MODELS}" >> $CL_DIR/local.settings

# add to local.settings the path to heideltime's settings
echo "# Configuration file used by HeidelTime (date extractor)" >> $CL_DIR/local.settings
echo "config_heideltime=${ABSOLUTEPATH_HEIDELTIME}/config-heideltime.props" >> $CL_DIR/local.settings

# add to config-heideltime.props the absolute path to TreeTagger
echo "\n\n#### GENERATED PARAMETERS\n" >> $CL_DIR/$HEIDELTIME_DIR/config-heideltime.props
echo "treeTaggerHome=${ABSOLUTEPATH_TT}" >> $CL_DIR/$HEIDELTIME_DIR/config-heideltime.props


cp $CL_DIR/local.settings core/src/main/resources
cp $CL_DIR/local.settings gui/WebContent/WEB-INF
