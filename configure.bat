set CURRENT_DIR=%cd%
set "CURRENT_DIR_SLASH=%CURRENT_DIR:\=/%"
set CACHE_DIR=cache
set TMP_DIR=tmp
set TT_DIR=treetagger
set TT_BIN=""
Set PYTHON_SCRIPTS_DIR=scripts
set HEIDELTIME_DIR=heideltime
set TT_BIN=binWindows
set CL_DIR=%CURRENT_DIR%\cl-working-dir
set CL_DIR_SLASH=%CURRENT_DIR_SLASH%/cl-working-dir

IF NOT EXIST %CL_DIR% ( mkdir %CL_DIR% )

ECHO "CL_DIR is :'%CL_DIR%'"

IF NOT EXIST %CL_DIR%\%CACHE_DIR%  ( mkdir  %CL_DIR%\%CACHE_DIR% )
IF NOT EXIST %CL_DIR%\%TMP_DIR% ( mkdir  %CL_DIR%\%TMP_DIR% )

python -m venv %CL_DIR%\cl_env
%CL_DIR%\cl_env\Scripts\python -m pip install --upgrade pip
%CL_DIR%\cl_env\Scripts\pip install -r requirements.txt -f https://download.pytorch.org/whl/torch_stable.html

IF NOT EXIST %CL_DIR%\%TT_DIR%\bin (mkdir %CL_DIR%\%TT_DIR%\bin)
IF NOT EXIST %CL_DIR%\%TT_DIR%\cmd (mkdir %CL_DIR%\%TT_DIR%\cmd)
IF NOT EXIST %CL_DIR%\%TT_DIR%\lib (mkdir %CL_DIR%\%TT_DIR%\lib)
IF NOT EXIST %CL_DIR%\%TT_DIR%\models (mkdir %CL_DIR%\%TT_DIR%\models)
IF NOT EXIST %CL_DIR%\%PYTHON_SCRIPTS_DIR% (mkdir %CL_DIR%\%PYTHON_SCRIPTS_DIR%)
IF NOT EXIST %CL_DIR%\%HEIDELTIME_DIR% (mkdir %CL_DIR%\%HEIDELTIME_DIR%)

xcopy core\lib\treetagger\%TT_BIN% %CL_DIR%\%TT_DIR%\bin
copy core\lib\treetagger\cmd\utf8-tokenize.perl %CL_DIR%\%TT_DIR%\cmd\
xcopy core\lib\treetagger\lib %CL_DIR%\%TT_DIR%\lib
xcopy core\lib\treetagger\models %CL_DIR%\%TT_DIR%\models

xcopy core\scripts %CL_DIR%\%PYTHON_SCRIPTS_DIR% /S

copy core\lib\heideltime\config-heideltime-no-treetagger.props %CL_DIR%\%HEIDELTIME_DIR%\config-heideltime.props

copy core\src\main\resources\parameter.settings %CL_DIR%\local.settings

set ABSOLUTEPATH_MODELS=%CL_DIR_SLASH%/%TT_DIR%/models
set ABSOLUTEPATH_TMP=%CL_DIR_SLASH%/%TMP_DIR%
set ABSOLUTEPATH_CACHE=%CL_DIR_SLASH%/%CACHE_DIR%
set ABSOLUTEPATH_PYTHON=%CL_DIR_SLASH%/cl_env
set ABSOLUTEPATH_PYTHON_SCRIPTS=%CL_DIR_SLASH%/%PYTHON_SCRIPTS_DIR%
set ABSOLUTEPATH_TT=%CL_DIR_SLASH%/%TT_DIR%
set ABSOLUTEPATH_HEIDELTIME=%CL_DIR_SLASH%/%HEIDELTIME_DIR%

rem add to local.settings the  absolute path to models
(ECHO.
ECHO.
ECHO #### GENERATED PARAMETERS
ECHO.) >> %Cl_DIR%\local.settings

rem add to local.settings the absolute path to tmp directory
ECHO # Temporary directory for ConnectionLens >> %CL_DIR%\local.settings
ECHO temp_dir=%ABSOLUTEPATH_TMP% >> %CL_DIR%\local.settings

rem add to local.settings the absolute path to the cache
ECHO # ConnectionLens cache location >> %CL_DIR%\local.settings
ECHO cache_location=%ABSOLUTEPATH_CACHE% >> %CL_DIR%\local.settings

rem add to local.settings the absolute path to python
ECHO # Python location >> %CL_DIR%\local.settings
ECHO python_path=%ABSOLUTEPATH_PYTHON%/bin/python >> %CL_DIR%\local.settings

rem add to local.settings the absolute path to python scripts
ECHO # Path to python scripts (flair and pdf) >> %CL_DIR%\local.settings
ECHO python_script_location=%ABSOLUTEPATH_PYTHON_SCRIPTS% >> %CL_DIR%\local.settings

rem add to local.settings the absolute paths to TreeTagger
ECHO # Treetagger location >> %CL_DIR%\local.settings
ECHO treetagger_home=%ABSOLUTEPATH_TT% >> %CL_DIR%\local.settings

ECHO # Stanford models location >> %CL_DIR%\local.settings
ECHO stanford_models=%ABSOLUTEPATH_MODELS% >> %CL_DIR%\local.settings

rem add to local.settings the path to heideltime's settings
ECHO # Configuration file used by HeidelTime (date extractor) >> %CL_DIR%\local.settings
ECHO config_heideltime=%ABSOLUTEPATH_HEIDELTIME%/config-heideltime.props >> %CL_DIR%\local.settings

rem add to config-heideltime.props the absolute path to TreeTagger
(ECHO.
 ECHO.
 ECHO #### GENERATED PARAMETERS
 ECHO.) >> %CL_DIR%\%HEIDELTIME_DIR%\config-heideltime.props
ECHO treeTaggerHome=%ABSOLUTEPATH_TT% >> %Cl_DIR%\%HEIDELTIME_DIR%\config-heideltime.props

copy %CL_DIR%\local.settings core\src\main\resources
copy %CL_DIR%\local.settings gui\WebContent\WEB-INF

