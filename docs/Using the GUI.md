We assume all the GUI installation is finished and you have opened a browser tab at the address: `http://localhost:8080/gui/`

## Selecting a database

You must start by selecting a database in the header drop-down menu.

![image](uploads/image1.png)

## Keyword search

The keyword search bar allows you to start a new keyword search using the algorithm in configuration property `global_search_algorithm`. The results of the keyword search are AnswerTree instances that are presented in the left panel.

![image](uploads/image2.png)

To add a tree to the main view, two methods:

* double-click on the tree, or
* click just once to select it, then click on the right chevron (NOTE: this method may not work on certain OSs) 

The "pie chart" button selects all trees in the list, and you can double-click on that button to add them all directly.

## The main view

Once you have added nodes to the main view, hover the mouse over a node to show its full name, its neighbors and its adjacent edges' labels. 

Right-click on a node will remove it from the view.
Nodes can be dragged (repositioned) with the left mouse button.
Clicking on a node will show its neighbors and context. 

## Adding neighbors

Clicking on a node opens two side panels.

The right panel shows the node's context.

The left panel allows selecting the neighbor nodes that are not yet present in the main view. As for the keyword search results, a neighbor can be clicked to select it; pressing the right chevron adds the selected neighbors to the main view.

The toolbar buttons allow selecting all the neighbors, or those of a certain node type (each type denoted by a color). As in the keyword search results, click on a toolbar button to select/deselect, double-click to add to the main view directly.

![image](uploads/image3.png)

## The legend

You can hover a item in the main view's legend to highlight the nodes of a certain type.

![image](uploads/image4.png)

A click on a legend item allows hiding nodes of a certain type: 

![image](uploads/image5.png)

## Main view settings

In the top toolbar, the layout settings menu allows adjusting the main view settings in real time

![image](uploads/image6.png)

The "Improve layout" button allows to recompute the placement of the nodes to improve the graph layout.

![image](uploads/image7.png)

## Saving, loading and exporting

- The "Export svg" button allows to export the main view to an svg file.
- The "Save visualization" button allows to save the main view to a file, including what database is used, what nodes are present and where, and the layout settings.
- The "Import visualization" allows you to import a file generated with "Save visualization". It will then connect automatically to the correct database, fetch the relevant graph information from the server, and add all these nodes to the main view.

![image](uploads/image8.png)
