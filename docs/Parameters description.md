Two kinds of parameters are used with ConnectionLens : **command line parameters**  and **configuration parameters** defined in the `.properties` file. *(Note that there are two properties files, one in the connection-lens/core and the other in the connection-lens/demo projects. To avoid problems, these files should be identical - it is possible to use different values but it's really not recommended unless you know why you're doing it.) *

# Command line parameters:

These parameters are used when calling the system in command line. They are used to control the behaviour of an execution (loading the data or querying it) or to generate different kinds of statistics. 

*  `-i | --input`: This parameter is used to specify the paths to the files that have to be loaded into a ConnectionLens graph. 
   *  **One file**: `-i file1.html`;
   *  **Multiple files**: separate file names by commas, e.g.: `-i file1.txt, json_file.json`.

*  `-o | --output`: This parameter is used to specify the path to an output file. If ommitted, the output is printed to STDOUT.

* `lateIndex`: if it's true, create necessary indexes at the begining of the loading. Indexes marked as `late` will be executed later in the loading process (issue https://gitlab.inria.fr/cedar/connection-lens/-/issues/314).

* `last`: when multiple loading are performed (with lateIndex paramater), this parameter indicates that this loading will be the last, it's important because with `latexIndex` parameter, some indexes will be not created but must be created before searching, this is why we need to know which loading is the last.

* `-ou` allows to associate an original URI to a loaded dataset (the path to the dataset being given by the -i option as explained above). For instance, we could use: 

`-ou https://www.facebook.com/Etienne-Chouard-178999005446283,http://twitter.com/Ruffin -i data/poc/2/fb-etienne-chouard.txt,data/poc/2/tweet-Ruffin.json` 


*  `-q | --query`: This parameter is used to specify a query to be solved by the system, e.g. "Assemblée", or "Assemblée Ruffin Chouard" (the former is a three-keyword query)

*  `-Q | --query-file`: This parameter is used to specify a file (list) of queries.

*  `-n | --noreset-at-start`: This parameter permit to preserve the original data.

*  `-a | --interactive`: Run ConnectionLens in an interactive (text-based) mode. In this mode, one can enter successive queries to a prompter and have them evaluated.

*  `-v | --verbose` Use verbose mode.

*  `-c | --config`: Path to configuration file.

*  `-x | --extractor`: Select the information extraction module to use. Currently we have one based on StanfordNLP and one based on FLAIR (which appears better).

To print out statistics, three command lines parameters can be used:

*  `-rs | --collect-registration-stats`: If this parameter is set, `logs registration-related` statistics will be collected and printed. These statistics include: compile-time properties (the version of CL, the branch used...), the configuration of the system (which OS is used, memory...), the config and the command line parameters and other statistics such as: the number of edges, nodes, the indexing time and the total execution time.
*  `-ss | --collect-similarity-stats`: If this parameter is set, all `similarity-related statistics` will be printed.
*  `-qs | --collect-query-stats`: If this parameter is set, all `query-related statistics` will be printed including these information:
* The statistics are only output when an experiment completes. In interactive mode (`-a` | `--interactive`), that is when the program hits EOF (Ctrl+D) and exits.

* `-f | --output-format`: affects how statistics and the environment are printed in the output, namely, default (tab-separated for copy/paste in spreadsheet), MARKDOWN (for pasting in .md files), and LATEX for pasting in tex files. The format defined in `-f` does not affect the additional output created by { `-v` | `--verbose` }, i.e. each answer is always serialized in JSON.

`-f`, `-a`, and `-v` can all be combined.

`INPUT`: the query;

`ANSW_NO`: the number of answers;

`Generated ATs`: the number of generated answer trees;

`Generated ATs by growing across`: the number of generated answer trees by growing across;

`Generated ATs by growing locally`: the number of generated answer trees by growing locally;

`Generated ATs by merge`: the number of generated answer trees by merge;

`MIN_T`: time spent in minimization;

`SRC_T`: all labels for time spent in none-specify part of the search algorithm ;

`MIN_T`: time spent to find the first result;

`TOTAL_T`: total time.
 
# Parameters given in the settings

ConnectionLens also uses a set of parameters found in [core/settings/local.settings](https://gitlab.inria.fr/cedar/connectionlens/-/blob/master/core/settings/local.settings) file.

There are several categories of parameters:

**DATABASE:** 

These govern the PostgreSQL database to use to store the graph. For example, the following parameters set the hostname, port, user, password and name of the DB:

```
RDBMSHost = localhost
RDBMSPort = 5432
RDBMSUser = kwsearch
RDBMSPassword = 
RDBMSDBName = cl_default
```

Please allow the RDBMSUser privileges to create or delete databases.

**TEMP DIRECTORY**
Directory where CL will write temporary files.
These include: 
- temporary files used for loading data in a relational database;
- dot-generated drawings of small CL graphs and answer trees;
- other temporary files created notably by the Flair NER tool. 

```
temp_dir=/tmp
```

**PYTHON PARAMETERS**
ConnectionLens uses Python scripts for tasks such as named entity extraction or PDF parsing. Following parameters are used to set them:

```
PYTHONPath=cl_env/bin/python3.6 (this is the path to the python installed as a virtual environment in your system)
python_script_location=scripts (location of the python scripts, present under core/scripts/)
```

**EXTRACTION**

Parameters here control entity extraction. `extractor` allows to chose the extractor: the **SNER (Stanford NER)** one is used by default, the **FLAIR_NER (Flair)** one is more accurate but requires some extra Python set-up steps and is slower, finally one can specify **NONE** to completely avoid entity extraction.

`extract_generics` allows controlling whether we want the so-called **generic** entity extraction (made by us in a rule-based fashion using the syntax and morphology of a phrase). If you use Flair as the main extractor, you probably not need the generic extraction.

`extract_from_uris` allows to state whether you want (or not) to extract entities from URI components.  In some cases we may want this (e.g., URIs of article titles which are long and contain many useful terms). In other cases we may want to avoid it (e.g., URIs of DBPedia which contain the name of the person, knowing also that the URI has a property called "name" leading to the same name in a more intelligible format). 


**Extraction policies** By default, ConnectionLens extracts entities from all text nodes (or even from URIs). If you want a more selective extraction, or you know that in a relatively structured database, some fields should always be considered e.g. as Person entity occurrences, you can achievee this using `extract_policy` (15/11/20: only supported so far for XML). 

* The value of `extract_policy` is a set of individual policies, comma-separated.

* Each individual policy is of the form **Path Decision**, and states what to do with text fields found under Path. If Decision is Person, Organization, or Location, each text found there will be treated as a Person (resp. Organization, Location). If Decision is NoExtract, then no extraction will be made on that text. If Decision is NoExtractAll, no extraction will be applied on this text, nor on any text found on a path that starts with Path.

* If we decided that a text field is of a certain type because of `extract_policy`, no other entity extraction will apply on it (not even generics etc.) *Policy-driven extraction has the highest priority.* 
 
* The policies from `extract_policy` that do not match a data source's structure (paths) will not impact extraction from that data source. 


The `max_extraction_cache_size`, is used to control the size of the size of the cache memory. 
        *  if the parameter = -1, nothing changed and all labels will be add to the memory cache 
        *  if this parameter > 0 in the current execution, limited number of labels (n) will be added to the cache memory (n <= max_extraction_cache_size) . 


`extractor_batch_size_cl`: this is the number of entries on which the extractor will be called (more efficient than calling once for each input). Use a power of 2; helpful values may be e.g., 128 for Flair.

`extractor_batch_size_flair`: This parameter will be used send to Flask web service, and then used by Flair to determine how many sentences to process in parallel.

***Disambiguation***
If `entity_disambiguation=true`, each Person, Organization, or Location entity will be confronted with a knowledge base and we will try to find for it an URI in that knowledge base. This is enabled by a Web service running outside of ConnectionLens, in the CEDAR cluster. If this parameter is set to false, no disambiguation will occur.

By default, disambiguation will be attempted for all Person, Organization and Location entity occurrences. This may take a long time and we may not want all these results. It is possible to control disambiguation at a finer granularity as follows using `entity_disambig_policy`. 

* Like the extraction policy, `entity_disambig_policy` is a list of comma-separated disambiguation policies. 

* Each disambiguation policy is a path paired with Person or Organization or Location. 

* An entity matches a dipol if it has the type specified by the dipol and it has been extracted from a text encountered on the dipol. 

* If `entity_disambig_policy` is not empty, we only disambiguate entities which match a dipol. Disambiguation will not be attempted for an entity not matching any dipol.

Observe that disambiguation is subordinated to extraction: we only disambiguate extracted entities. Further, disambiguation is orthogonal to extraction, that is: extracted entities are disambiguated regardless of the extractor that produced them (SNER, Flair, or policy-driven). 

<!--**The overall node creation in ConnectionLens is possibly best described in this diagram:** 
[textNodesInCL.pdf](uploads/d30f7a5610d054a338cf20c310ad37a1/textNodesInCL.pdf)
(also [pptx sources](uploads/4082d04ce0dc7b31e7a6bfcc813139cc/textNodesInCL.pptx))-->

***PDF extraction***

Two parameters are used to control the pdf extraction process:
 
   * 'flavor' is an option with two values: `stream` and `lattice`.  By default, the lattice value is used. If the input contains a table without visible lines (separators), use stream instead. 

   * 'type_pdf' is an option stating which class this PDF file belongs to. Possible values are: `generic`, `paper`, `cois_EFSA`. Generic will handle any PDF in best-effort and is the default value. "paper" is for scientific papers with author information at the beginning and acknowledgments, COI etc. at the end. Finally, "cois_EFSA" is specific to PDF files issued by EFSA which publish specific interest declarations. 
   
   **DRAWING AND PLOTTING:**
 This part contains all information needed for drawing the ingested graph or query results. There are 4 main parameters:
  - `drawing.draw`: set drawing to true or false; if false, nothing will be drawn (thus the values of the other drawing-related parameters will be basically ignored); 
  - `drawing.dot_installation`: the path where the (optional) [DOT/Graphviz](https://www.graphviz.org/) software is installed. It is needed in order to get answer trees drawn when running the code in comand line;
  - `drawing.coarse_edge`: if true, so-called `coarse` edges (those that connect nodes directly to their enclosing dataset(s)) will be included in drawings. Otherwise, they will be ommitted. 

Drawing directories files is moved to `temp_dir`

**VALUE NODE CREATION:**
In principle a node is created in CL for any relational attribute value, or content of an XML or HTML node, or JSON value, or text, or RDF literal.

If a text is too long, it is better to split it in several texts. Whether we split or not is dictated by `split_sentences`. The maximum length of a split is `max_label_length`. 

Furthermore,  it is possible to factorize leaf (value) node creation using `value_atomicity`. If it is `PER_INSTANCE`, we get thee above behavior (1 node for each occurrence). If it is `PER_DATASET` there will be only value (leaf) one node with a given label in a given dataset. If it is `PER_GRAPH` there will be only one per graph. Factorizing creates less nodes and densifies the graph. It appears a good idea unless it may lead to unwanted connections (e.g., connecting all the "N/A"s of a graph into one). 

**INDEXING**

A parameter allows to select an indexing strategy (how are search terms extracted, and how are they associated to nodes and edges that contain them in a ConnectionLens graph). Currently we support indexing through PostgreSQL's full text indexing functionality and through Lucene.



**NODE COMPARISON AND SIMILARITY MEASURES**

Parameters under this section control the comparisons made among nodes to determine which pairs of nodes should be connected by a sameAs edge.

First, it is possible to avoid comparisons completely.

Second, there are in fact several comparisons made, i.e., Person entities are compared with Person entities, Locations with Locations, short strings with short strings and long strings with long strings etc. The string comparisons tend to be the most numerous, the most costly, and the least useful, because our main goal is rather to connect on common entities. Therefore, a separate parameter allows to control whether to perform string comparisons or not.  

Next, different score functions are defined for different groups of nodes to be compared; and different thresholds are defined for each score function, in order to decide at which threshold we create a sameAs edge.  

**SEARCH**

Parameters under this section are used to select the algorithm that searches for query answers.

*`query_only_specific_edge` this parameter allowed the `GAMSearch` algorithm to only traverse edges that have a high specificity during the searching procedure to accelerate the search

**GRAPH STORAGE**

A parameter here allows to select the method used to store the graph. Currently there is only one method supported (COMPACT).

**ABSTRACTION:**  
These parameters control whether or not we build an *abstract* graph. To build an abstract graph, set the property ``create_abstract_graph`` to ``true``. If the property ``drawing.draw`` is ``true``, then the abstract graph will be printed along with the cl graph in the drawing directory, if it is not too large, using a dot and an html method. If the property ``-json`` is used when calling the demo, the abstract graph will be exported to json along with the cl graph. If the property ``-rs`` is used when calling the demo, abstraction stats will be collected on the abstraction.


**CACHES:**  

Currently our graphs are stored in a persistent store (in particular, in PostgreSQL). For more efficiency, parts of the graphs which have been accessed by the software are cached in memory. Parameters under this category control various cache parameters. They are likely to have an impact on performance, but they should have no impact on the correctness (result) of the code. 


<!--# Different settings files

Currently we have:

* **ciserver.settings** which are the settings on the continuous integration server. These should not be changed unless the code substantially evolves.
* **local.settings** are the settings every user can modify.
* justLoad.settings should be copied into a user's local.settings to load a graph in a very "bare-bones" mode (no extraction, no disambiguation, ...)
* **loadExtractLink.settings** specifies the right options in order to load the graph and extract entities. You still have to chose the language, and the extractor. 
* **loadExtractLinkDisambiguate.settings** shows how to load, extract, and disambiguate the graph. You still have to chose the language, and the extractor. -->

