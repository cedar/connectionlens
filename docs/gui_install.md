# Running

The GUI can be run through a Web server or from a J2EE Web application container.

## Running directly through a Web server

Install a Web server, such as [Tomcat 9.0](https://tomcat.apache.org/download-90.cgi)

- In the Tomcat installation directory, there is a directory called `webapps`. 
- Copy `connectionlens/gui/target/gui.war` in Tomcat's `webapps` directory.
- Copy the `gui/WebContent/WEB-INF/local.settings` file to `webapps/gui/WEB-INF/` directory of Tomcat. This will make the GUI run with exactly the same set of parameters as that from the command line. If you want to change any parameter, you can update here.

The application will be accessible from:

  http://hostname[:8080]/gui/

# Configuring the GUI project
The configuration file located in your tomcat `webapps/gui/WEB-INF/local.settings` and follows the same syntax as the file from the main project. Changes to the file will not take effect at runtime, so do not forget to restart the server after any changes.

*Note: If you have loaded data directly through the main project, be sure to use the same location for caches (property `cache_location`) in the two files, to ensure that behaviors are consistent between the main project and the GUI.* 

# Interacting with the GUI

Details on using the GUI are provided [here](https://gitlab.inria.fr/cedar/connectionlens/-/blob/master/docs/Using%20the%20GUI.md).
 
# Known issues
1. To avoid encoding issues using Tomcat on Windows, it may be necessary to add this line to `catalina.bat`:

  set CATALINA_OPTS=-Dfile.encoding="UTF-8"

2. The Web navigators that are part of J2EE environments such as Eclipse Enterprise Edition are not as reliable and robust as standalone navigators. Try using a major browser such as Chrome, Safari or Firefox (the latter seems the most robust).
