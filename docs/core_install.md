## Installation

### Run the script `configure.sh/configure.bat`:

**Please note that for this step your python3 installation should point to Python3.6 or Python3.7.**

From the `connectionlens` directory, run:
- `sh configure.sh` on Linux and MacOS;
- `configure.bat` on Windows.

**Running the configure script once after cloning the ConnectionLens repository is required.** This will:
- create directories to store ConnectionLens resources and write auxiliary files;
- create the main settings file, named `local.settings` and located under `core/src/main/resources` - this file allows you to tune ConnectionLens parameters.

### PostgreSQL

A PostgreSQL server must be running locally, and you must have access to an account with the ability to create users. By default, the username is `kwsearch`, therefore this user must be created beforehand. 

If needed, manually update the `RDBMS_password` property with your postgres password in the `local.settings` file present under `core/src/main/resources/` and `gui/WebContent/WEB-INF/`.

### Getting ready

**You can now run ConnectionLens using the steps mentioned in `README.md`. Do not forget to add `-c src/main/resources/local.settings` to your Java command in order to let ConnectionLens locate its main settings file.**

If ConnectionLens raise the error `A value for parameter RDBMS_DBName was passed, but this parameter is not supported.` or does not find entity extractors, you may have forgotten to:
- run `configure.sh` or `configure.bat` depending on your OS;
- specify `-c src/main/resources/local.settings` in your Java parameters.


## Entity extractors

As of March 2020, we have **two** alternative entity extractors, as explained below.

### Stanford Core NER

Currently, the **default** entity extractor uses the Stanford Core NLP infrastructure, which needs access to the language-specific model files in a location reachable by the system. It makes use of TreeTagger, which is installed in `path_to_cl_directory/treetagger`

We currently support French and English. The default model files locations are in `path_to_cl_directory/treetagger/models`, but it is possible to choose another directory and indicate it in `local.settings` as the value of the parameter `stanford.models`:
	
	`path_to_cl_directory/treetagger/models/elra.ser.gz` (for French)
	`path_to_cl_directory/treetagger/models/english.all.3class.distsim.crf.ser.gz` (for English)
	
A simple rule-based entity extraction may also be performed to extract additional entities with some specific properties (ex. person names not extracted by the entity extractor), for this we used the Stanford constituency parser which is also a part of the Stanford CoreNLP Tools. For this step some others models will be included for French and English:
	
    `path_to_cl_directory/treetagger/models/french.tagger` (French tagging)
    `path_to_cl_directory/treetagger/models/english.tagger` (English tagging)
    `path_to_cl_directory/treetagger/models/frenchSR.ser.gz` (French Parsing)
    `path_to_cl_directory/treetagger/models/englishSR.ser.gz` (English Parsing)
    
    

### Flair NLP NER

Another entity extractor (https://gitlab.inria.fr/cedar/cl_extraction_simplified) is based on the Flair NLP library and gives better results. This needs a python installation and some libraries that allow running the python script used to extract entities from text. ConnectionLens makes use of a virtual environment for python which is located in `path_to_cl_directory/cl_env/bin/python3.6`. The folder [scripts](https://gitlab.inria.fr/cedar/connectionlens/-/tree/master/core/scripts/) contains Python scripts for Flair NER and content extraction from PDF files. 

By default, the **`Stanford NER (SNER)`** is used to extract entities `extractor=SNER`. 

If you want to switch to `Flair NER`, you need to change the `extractor` parameter in the `local.settings` file to:

`extractor=FLAIR_NER`

### Language

For both extractors, the language is defined by the `default_locale` in `local.settings` file. 
The system supports `fr` for French and `en` for English. All documents loaded together are assumed to be in the same language 
(English or French, depending on the value of the default_locale). 



<!--## TreeTagger
TreeTagger must be installed, with the parameter `treetagger_home` pointing to the directory,
and a directory called `models` under that same directory.
In `models`, you must have  the **french.par** and **english-utf8.par** parameter files 
available from  models folder or from https://files.inria.fr/cedar/models.zip .
Treetagger is available from [http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/](http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/)
There are two models for English; the first (PENN) worked well for us.

## Entity extractor

As of March 2020, we have **two** alternative entity extractors, as explained below.

### Stanford Core NER

Currently, the **default** entity extractor used the Stanford Core NLP infrastructure, which needs 
access to the language-specific model files in a location reachable by the system. 

We currently support French and English. The default model files locations are in `/var/connectionlens/models/`, but it is possible to choose another directory and indicate it in `local.settings` as the value of the parameter `stanford.models`:
	
	/var/connectionlens/models/elra.ser.gz (for French)
	/var/connectionlens/models/english.all.3class.distsim.crf.ser.gz (for English)
	
A simple rule-based entity extraction may also performed to extract additional entities with some specific properties (ex. person names not extracted by the entity extractor), for this we used the Stanford constituency parser which is also a part of the Stanford CoreNLP Tools. For this step some others models will be included for French and English:
	
    /var/connectionlens/models/french.tagger (French tagging)
    /var/connectionlens/models/english.tagger (Fnglish tagging)
    /var/connectionlens/models/frenchSR.ser.gz (French Parsing)
    /var/connectionlens/models/englishSR.ser.gz (English Parsing)
    
This files can be downloaded from  models folder or from https://files.inria.fr/cedar/StanfordModels.zip .

### Flair NLP NER

A new entity extractor (https://gitlab.inria.fr/cedar/cl_extraction_simplified) developed by **Catarina Conceição**  is based on the Flair NLP library and gives better results.

This new extractor needs a python installation and some libraries that allow running the python script used to extract entities from text.

Before using this extractor you need to follow these installation instructions on your local machine:

***Install python:***

Install **python 3.6.5**. In order to install the Python libraries that support Flair extractor and PDF integration, please first create a virtual environement using the following command: 

```
python3.6 -m venv cl_env
```

This will create a folder cl_env under your current directory. Then, activate the virtual environment using:

```
source cl_env/bin/activate
```

***Install the python libraries required for Flair NLP NER and PDF integration*** from inside the virtual environment by typing: 

```
pip install -r requirements.txt
```

In the **`core/settings/local.settings`** file update the `PYTHONPath` parameter with the path to your virtual python installation located at:

```
cl_env/bin/python3.6
```
-->

<!--***Install rust :***

The ```tokenizers``` package of Flair requires `rust`:  

```
curl https://sh.rustup.rs -sSf | sh -s -- -y
```

Then add to your path `.cargo/bin`, for instance by typing this: 

```
export PATH="$HOME/.cargo/bin:$PATH"
```

or adding it to your .tcshrc or .bashrc file.-->


<!--

# Locations of the Flair and PDF integration scripts
 
The folder [scripts](https://gitlab.inria.fr/cedar/connectionlens/-/tree/master/core/scripts/) contains Python scripts for Flair NER and content extraction from PDF files. By default, ConnectionLens expects these two folders to be under `/var/connectionlens/scripts`; you can chose another directory and indicate it in `core/setting/local.settings` as the value of the parameter `python_script_location`. You need to move the directories  `Flair_NER_tool` and `pdf_scripts` under `/var/connectionlens/scripts/` (or the alternative directory you chose for the scripts).
### Language

For both extractors, the language is defined by the `default_locale` in `local.settings` file. 
The system supports `fr` for French and `en` for English. All documents loaded together are assumed to be in the same language 
(English or French, depending on the value of the default_locale). 


# Optional: drawing graphs and query answers using DOT
Dot (or GraphViz) is a practical tool for drawing graphs of moderate size. It can be downloaded from: [https://www.graphviz.org/](https://www.graphviz.org/).

ConnectionLens does not require it, but if available, and if you set **drawing.draw=true** in the [local.settings](https://gitlab.inria.fr/cedar/connection-lens/blob/develop/core/src/main/resources/local.settings) file, it will use Dot to generate visualizations of small graph or answer trees.

If you install Dot on your machine, specify the path to the dot executable as the value of the parameter **drawing.dot_installation** in the [local.settings](https://gitlab.inria.fr/cedar/connection-lens/blob/develop/core/src/main/resources/local.settings) file. -->

